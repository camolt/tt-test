<div id="terms">
    <ul>
        <li><a href="#">30-DAY RETURNS</a></li>
        <li><a href="#">100% REFUND</a></li>
        <li><a href="#">FREE RETURNS</a></li>
        <li><a href="#">LIFETIME WARRANTY</a></li>
        <li><a href="#">FREE SHIPPING</a></li>
    </ul>
</div>

<header class="container">
    <div class="row row-one">
        <div class="column column-one col-sm-4 col-xs-12">
            <ul>
                <li><i class="fa fa-map-marker"></i><a href="#">Location</a></li>
                <li><i class="fa fa-calendar"></i><a href="#">Appointent</a></li>
            </ul>
        </div>

        <div class="column column-two col-sm-6 col-sm-offset-2 col-md-offset-2 col-xs-12">
            <ul>
                <li><i class="fa fa-user"></i><a href="#">Login</a></li>
                <li><i class="fa fa-heart"></i><a href="#">Wishlist <span>(0)</span></a></li>
                <li><i class="fa fa-cart-plus"></i><a href="#">Cart <span>(0)</span></a></li>
            </ul>
        </div>
    </div>

    <div class="row row-two">
        <div class="column-one col-md-3 col-xs-12 hidden-sm hidden-xs">
            <i class="fa fa-phone"></i><span>800-WE-JEWEL</span>
        </div>

        <div class="column column-two col-md-6 col-xs-12">
            <h1><i class="fa fa-diamond"></i>Your Store Name</h1>
        </div>

        <div class="column-one col-md-3 col-xs-12 col-sm-4 hidden-xs visible-sm">
            <i class="fa fa-phone"></i><span>800-WE-JEWEL</span>
        </div>

        <form class="column column-three col-sm-4 col-sm-offset-4 col-md-2 col-md-offset-1 col-xs-12 hidden-xs" role="search">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search">

                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
    </div>
</header>

<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <form class="visible-xs" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">

                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </form>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="#">Diamonds</a>
                </li>
                <li>
                    <a href="#">Engagement Rings</a>
                </li>
                <li>
                    <a href="#">Wedding Rings</a>
                </li>
                <li>
                    <a href="#">Fine Jewelry</a>
                </li>
                <li>
                    <a href="#">Watches</a>
                </li>
                <li>
                    <span class="spacer hidden-xs"></span>
                    <a href="#">Education</a>
                </li>
                <li>
                    <a href="#">Contact</a>
                </li>
                <li>
                    <a href="#">About</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
