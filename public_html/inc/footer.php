<footer>
    <div class="container">
        <div class="row row-one">
            <div class="column-one col-md-3 col-xs-12">
                <h3>THE SHOP</h3>
                <p>Your brand has an amazing story which needs to be told. Summarize whatyour brand brings to the table for the consumer. 
                    Include keywords in your summary which tells search engines what searches your website should show for.
                </p>
            </div>

            <div class="column-two col-md-2 col-xs-12">
                <h3>CONTACT</h3>
                <ul>
                    <li>844.WE.JEWEL</li>
                    <li>844.935.3936</li>
                    <li>224 SW First Avenue</li>
                    <li>Portland, OR 97204</li>
                    <li>Map / Directions</li>
                </ul>
            </div>

            <div class="links column-three col-md-offset-1 col-md-2 col-sm-6 col-xs-12">
                <h3>COLLECTIONS</h3>
                <ul>
                    <li><a href="#">Engagement Rings</a></li>
                    <li><a href="#">Wedding Bands</a></li>
                    <li><a href="#">Diamonds</a></li>
                    <li><a href="#">Fashion Jewelry</a></li>
                    <li><a href="#">Watches</a></li>
                </ul>
            </div>

            <div class="links column-four col-md-3 col-md-offset-1 col-sm-6 col-xs-12">
                <h3>CUSTOMER SERVICES</h3>
                <ul>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Services</a></li>
                    <li><a href="#">Education</a></li>
                    <li><a href="#">Contact</a></li>
                    <li><a href="#">Your Account</a></li>
                </ul>
            </div>
        </div>

        <div class="row row-two">
            <div class="column-one col-md-5 col-sm-6 col-xs-12">
                <ul>
                    <li><a href="#">&COPY; Your Store Name</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Terms of Use</a></li>

                </ul>
                <p>Jewelry Website Design by Thinkspace Jeweler</p>
            </div>

            <div class="column-two col-md-offset-3 col-md-4 col-sm-6 col-xs-12">
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-camera"></i></a>
                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-linkedin"></i></a>
            </div>
        </div>
    </div>
</footer>

