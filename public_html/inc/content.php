<section id="jumbo">
    <div class="container">
        <h1>SHOP OUR <span>TOP 20</span> ENGAGEMENT RINGS</h1>
    </div>
</section>

<section id="logos" class="container">
    <img src="media/images/logo-john-hardy.png" alt="John Hardy">
    <img src="media/images/logo-simon-g.png" alt="Simon G">
    <img src="media/images/logo-mikimoto.png" alt="Mikomoto">
    <img src="media/images/logo-verragio.png" alt="Verragio">
    <img src="media/images/logo-gabriel.png" alt="Gabriel">
</section>

<section id="collections" class="container-fluid">  
    <div class="row row-one">
        <div class="column column-one col-sm-6 col-xs-12">
            <div class="wrapper">
                <h1 class="underline">NEW COLLECTION</h1>
                <h3>Valentien's Day Selections</h3>
                <p>Fusce sed ligula nec nunc porta dapibus. Ut sapien velit, 
                    sollicitudin et lorem sit amet, auctor auctor eros. Proin 
                    nunc libero, pulvinar vitae pulvinar id, sollicitu din vitae sem. 
                    Sed rhoncus leo sit amet tortor dignissim. Ut saapien velit, 
                    sollicitudin et lorem sit amet, auctor. Fusce sed ligula nec 
                    nunc porta dapibus.
                </p>
                <a href="#" class="btn round">READ MORE</a>     
            </div>
        </div>    
        <div class="column column-two col-sm-6 col-xs-12"></div>
    </div>

    <div class="row row-two">
        <div class="column column-one col-sm-6 col-xs-12"></div>     
        <div class="column column-two col-sm-6 col-xs-12">
            <div class="wrapper">
                <h1 class="underline">NEW COLLECTION</h1>
                <h3>Valentien's Day Selections</h3>
                <p>Fusce sed ligula nec nunc porta dapibus. Ut sapien velit, 
                    sollicitudin et lorem sit amet, auctor auctor eros. Proin 
                    nunc libero, pulvinar vitae pulvinar id, sollicitu din vitae sem. 
                    Sed rhoncus leo sit amet tortor dignissim. Ut saapien velit, 
                    sollicitudin et lorem sit amet, auctor. Fusce sed ligula nec 
                    nunc porta dapibus.
                </p>
                <a href="#" class="btn round">READ MORE</a>
            </div>
        </div>
    </div>
</section>

<section id="products-display">
    <div class="slider">
        <ul>
            <li class="sale">
                <div class="figure">
                    <img src="media/images/slider-neklace.jpg" alt="Whie Gold Neklace">
                    <div class="code">117254-100</div>
                    <div class="figcaption">WHITE GOLD NEKLACE</div>
                    <div class="underline"></div>
                    <div class="price">$1500.00</div>
                </div>
            </li>

            <li>
                <div class="figure">
                    <img src="media/images/slider-ring-black-stone.jpg" alt="Onyx White Gold Ring">
                    <div class="code">117254-100</div>
                    <div class="figcaption">ONYX WHITE GOLD RING</div>
                    <div class="underline"></div>
                    <div class="price">$1500.00</div>
                </div>
            </li>

            <li>
                <div class="figure">
                    <img src="media/images/slider-neklace-long.jpg" alt="Pearl & Onyx Pendant">
                    <div class="code">117254-100</div>
                    <div class="figcaption">PEARL & ONYX PENDANT</div>
                    <div class="underline"></div>
                    <div class="price">$1500.00</div>
                </div>
            </li>

            <li>
                <div class="figure">
                    <img src="media/images/slider-weave-ring.jpg" alt="Whie Gold Ring">
                    <div class="code">117254-100</div>
                    <div class="figcaption">WHITE GOLD RING</div>
                    <div class="underline"></div>
                    <div class="price">$1500.00</div>
                </div>
            </li>
        </ul>
    </div>
</section>

<section id="diamonds">
    <h2 class="underline">SHOP DIAMONDS</h2>
    <figure>
        <?php include('media/svg/Round.php'); ?>  
        <figcaption>ROUND</figcaption>
    </figure>
    <figure>
        <?php include('media/svg/Princess.php'); ?>  
        <figcaption>PRINCESS</figcaption>
    </figure>
    <figure>
        <?php include('media/svg/Radiant.php'); ?>  
        <figcaption>RADIANT</figcaption>
    </figure>
    <figure>
        <?php include('media/svg/Asscher.php'); ?>  
        <figcaption>ASSCHER</figcaption>
    </figure>
    <figure>
        <?php include('media/svg/Cushion.php'); ?>  
        <figcaption>CUSHION</figcaption>
    </figure>
    <figure>
        <?php include('media/svg/Oval.php'); ?>  
        <figcaption>OVAL</figcaption>
    </figure>
    <figure>
        <?php include('media/svg/Emerald.php'); ?>  
        <figcaption>EMERALD</figcaption>
    </figure>
    <figure>
        <?php include('media/svg/Pear.php'); ?>  
        <figcaption>PEAR</figcaption>
    </figure>
    <figure>
        <?php include('media/svg/Marquise.php'); ?>  
        <figcaption>MARQUISE</figcaption>
    </figure>
    <figure>
        <?php include('media/svg/Heart.php'); ?>  
        <figcaption>HEART</figcaption>
    </figure>  

</section>

<section id="articles" class="container" >
    <div class="row row-one">
        <article class="col-md-6 col-xs-12 column-one">
            <figure class="col-md-4 col-sm-2 col-xs-8"><img src="media/images/thumb-ring-blue-stone.jpg" alt="Blue Stone Ring"></figure>
            <div class="excerpt col-md-8 col-sm-10 col-xs-12" > 
                <h4>Designer Name Trunk Show At Your Store Name</h4>
                <p>Your Store Name Invites you to a Holiday
                    Trunk Show featuring Collection’s Fine Jewelry
                    Designer Name Here. Be sure to RSVP on our
                    Facebook Page. Join our E-Mail Club to receive
                    exclusive special offers for this event.
                </p>

            </div>
        </article>

        <article class="col-md-6 col-xs-12 column-two">
            <figure class="col-md-4 col-sm-2 col-xs-8"><img src="media/images/thumb-jewelry-sack.jpg" alt="Jewelry Sack"></figure>
            <div class="excerpt col-md-8 col-sm-10 col-xs-12" > 
                <h4>Your Store Name 2016 Gift Guide Unique Gifts Starting At $75</h4>
                <p>Timeless jewelry and gifts that never go out of
                    style. Unique gifts starting at $75. If you are looking
                    for a perfect gift for that special someone this is the
                    place to get started. Contact us today and let us be
                    your personal shopper and have some special
                    options ready for your arrival.
                </p>
            </div>
        </article>
    </div>

    <div class="row row-two">
        <article class="col-md-6 col-xs-12 column-one">
            <figure class="col-md-4 col-sm-2 col-xs-8"><img src="media/images/thumb-lovers.jpg" alt="Lovers"></figure>
            <div class="excerpt col-md-8 col-sm-10 col-xs-12" > 
                <h4>Proposal & Engagement Ring Workshop Every Tuesday</h4>
                <p>Choosing the perfect engagement ring that
                    is social media worthy and within your budget
                    can be a difficult process. Join our Proposal
                    & Engagement Ring Workshop every Tuesday
                    night from 7:00 - 8:00pm for expert advice.
                </p>
            </div>
        </article>

        <article class="col-md-6 col-xs-12 column-two">
            <figure class="col-md-4 col-sm-2 col-xs-8"><img src="media/images/thumb-golden-bracelet.jpg" alt="Golden Bracelet"></figure>
            <div class="excerpt col-md-8 col-sm-10 col-xs-12"> 
                <h4>Your Store Name Winner of 2016 Offical Jewelry Organization</h4>
                <p>The Official Jewelry Organization has announced
                    the winners of the prestigious Amazing OJO Award
                    for 2016. The 42nd annual event was held in Las
                    Vegas and is regarded as the world's preeminent
                    authority on all things jewelry ... period.
                </p>
            </div>
        </article>
    </div>
</section>

<section id="craftsmanship">
    <div class="container">
        <div class="row">
            <h1 class="underline">OUR CRAFTSMANSHIP</h1>
            <h3>Exceptional Attention To Detail</h3>
            <p>Fusce sed ligula nec nunc porta dapibus. Ut sapien velit, sollicitudin et lorem sit amet, auctor auctor eros.
                Proin nunc libero, pulvinar vitae pulvinar id, sollicitu din vitae sem. Sed rhoncus leo sit amet tortor dignissim.
            </p>
            <img src="media/images/diamond-picker.jpg" alt="Diamonds">
        </div>
    </div>
</section>

<section id="you" class="container-fluid">
    <h1 class="underline"><i class="fa fa-camera"></i> # YOU HASHTAG</h1>
    <p>Fusce sed ligula nec nunc porta dapibus. Ut sapien velit, sollicitudin et lorem sit amet, auctor auctor eros.
        Proin nunc libero, pulvinar vitae pulvinar id, sollicitu din vitae sem. Sed rhoncus leo sit amet tortor dignissim.
    </p>
    <div class="row">
        <img class="col-md-3 col-xs-12" src="media/images/footer-grafe-ring.jpg" alt="Grafe Ring">
        <img class="col-md-3 col-xs-12" src="media/images/footer-golden-ring.jpg" alt="Golden Ring">
        <img class="col-md-3 col-xs-12" src="media/images/footer-silver-ring.jpg" alt="Silver Ring">
        <img class="col-md-3 col-xs-12" src="media/images/footer-pendants.jpg" alt="Silver Pendants">
    </div>
</section>

