<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Shop template #1</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Jewelry online shop"/>
        <meta charset="utf-8">
    
        <!-- CSS
        ================================================== -->
        
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/styles.css"> 

        <!-- FONTS
        ================================================== -->
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
        
        <!-- Favicons
        ================================================== -->
        <link rel="shortcut icon" href="media/images/favicon.png">    
    </head>
    
    <body>
        <div class="container-main">
            <?php include('inc/header.php'); ?>  
            <?php include('inc/content.php'); ?>
            <?php include('inc/footer.php'); ?>

            <script src="js/libs/jquery.min.js" type="text/javascript"></script>
            <script src="js/libs/bootstrap.collapse.js" type="text/javascript"></script>
            <script src="js/shop-template-app.js" type="text/javascript"></script>   
        </div>
    </body>
    
</html>